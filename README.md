# Obsessive Facts Ansible Playbooks

This example repository contains [Ansible playbooks](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_intro.html)
that could be used to deploy the Obsessive Facts web site. These are
tested and working on Ubuntu Server 22.04 LTS (both the AMD64 and ARM
versions). There are
three playbooks you can use:

1. **server.yml:** performs initial configuration of the web server and
   should be run first.
2. **obsessivefacts.yml:** installs the Obsessive Facts website on your
   server. This repo contains fake placeholders for the SSL key file,
   so you should go get your own or something.
3. **obsessivefacts-restore-backup.yml:** restores a database backup of
   the Obsessive Facts website. Note that the backup contained in this
   repo has private information truncated but it should be sufficient
   to get a working "blank" installation going.

## Initial Setup

Take note of the environment variables in `env.example`. This is just
an example of what variables we need to configure the Postgres database.
Copy this to a file called `.env`, make it executabe (`chmod +x .env`)
and make sure you put some better usernames and passwords in there.

You must have Ansible installed and configured with groups for `server`
and `obsessivefacts` listed in the [hosts inventory](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html), eg. substitute your actual IP address and SSH port in the example below:
```
[server]
192.168.1.249:22

[obsessivefacts]
192.168.1.249:22
```
## How to run

1. `source .env` assuming you set this up as described.

2. `ansible-playbook server.yml --ask-become-pass`
   (always run this first)

3. `ansible-playbook obsessivefacts.yml --ask-become-pass`
   
   Note you'll have to comment out the stuff about certbot from _main.yml_
   until you can run certbot on your real server to get a real certificate.
   Please [see my blog post for detailed instructions](https://www.obsessivefacts.com/blog/2023-06-07-how-to-self-host-your-node-js-website.html#step-11-enable-ssl-and-make-it-persist-to-future-ansible-deploys).
   Placeholders for these files are in:
   
   - `./obsessivefacts/files/0000_csr-certbot.pem`
   - `./obsessivefacts/files/0000_key-certbot.pem`
   - `./obsessivefacts/files/cert1.pem`
   - `./obsessivefacts/files/chain1.pem`
   - `./obsessivefacts/files/fullchain1.pem`
   - `./obsessivefacts/files/meta.json`
   - `./obsessivefacts/files/obsessivefacts.com.conf`
   - `./obsessivefacts/files/options-ssl-nginx.conf`
   - `./obsessivefacts/files/private_key.json`
   - `./obsessivefacts/files/privkey1.pem`
   - `./obsessivefacts/files/regr.json`
   - `./obsessivefacts/files/ssl-dhparams.pem`
   
   Maybe you will also need to add your real GitLab private key to do the
   git checkout part, replacing the placeholder files:

   - `./obsessivefacts/files/id_rsa`
   - `./obsessivefacts/files/id_rsa.pub`

4. `ansible-playbook obsessivefacts-restore-backup.yml --ask-become-pass`
   
   This restores the PostgreSQL database for Obsessive Facts. Only run it
   once. If you run it more than once there's a chance it can mess up the
   primary key sequences for some of the higher-traffic tables. If you do
   this and the app starts crashing you can run the SQL commands in
   `./obsessivefacts-restore-backup/files/rescue.sql` to un-fuck everything.
