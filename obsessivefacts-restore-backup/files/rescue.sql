-- run this manually on your obsessivefacts database
-- if you accidentally run the obsessivefacts-restore-backup.yml multiple times.

SELECT setval(pg_get_serial_sequence('fmiffel_favorite', 'id'), coalesce(MAX(id), 1)) from fmiffel_favorite;
SELECT setval(pg_get_serial_sequence('fmiffel_follow', 'id'), coalesce(MAX(id), 1)) from fmiffel_follow;
SELECT setval(pg_get_serial_sequence('fmiffel_post', 'id'), coalesce(MAX(id), 1)) from fmiffel_post;
SELECT setval(pg_get_serial_sequence('fmiffel_tag', 'id'), coalesce(MAX(id), 1)) from fmiffel_tag;
SELECT setval(pg_get_serial_sequence('fmiffel_user', 'id'), coalesce(MAX(id), 1)) from fmiffel_user;
SELECT setval(pg_get_serial_sequence('guestbook', 'id'), coalesce(MAX(id), 1)) from guestbook;
SELECT setval(pg_get_serial_sequence('plugin', 'id'), coalesce(MAX(id), 1)) from plugin;
SELECT setval(pg_get_serial_sequence('post', 'id'), coalesce(MAX(id), 1)) from post;
SELECT setval(pg_get_serial_sequence('role', 'id'), coalesce(MAX(id), 1)) from role;
SELECT setval(pg_get_serial_sequence('sillyputty_block', 'id'), coalesce(MAX(id), 1)) from sillyputty_block;
SELECT setval(pg_get_serial_sequence('sillyputty_state', 'id'), coalesce(MAX(id), 1)) from sillyputty_state;
SELECT setval(pg_get_serial_sequence('public.user', 'id'), coalesce(MAX(id), 1)) from public.user;